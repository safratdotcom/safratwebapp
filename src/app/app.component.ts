import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import * as firebase from 'firebase';

// Declare ga function as ambient
declare let ga: Function;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  firstRun: boolean = true;
  @ViewChild(Nav) nav: Nav;

  constructor(private platform: Platform, private statusBar: StatusBar, private splashScreen: SplashScreen) {

    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyCZmVL81yP5nt1HqEXC3YA28dv2rSWtyxs",
      authDomain: "safratwebapp.firebaseapp.com",
      databaseURL: "https://safratwebapp.firebaseio.com",
      projectId: "safratwebapp",
      storageBucket: "safratwebapp.appspot.com",
      messagingSenderId: "779918290139"  
    };
    firebase.initializeApp(config);
    this.platform.setDir('rtl', true);
  }

  ngAfterViewInit() {

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        // User is authenticated.
        this.setRootPage('HomePage');
        console.log('in HomePage');
      } else {
        // User is not authenticated.
        this.setRootPage('HomePage');
        console.log('in HomePage');
        //this.setRootPage(LoginPage);
        //console.log('in LoginPage');
      }
    });
  }

  setRootPage(page) {

    if (this.firstRun) {

      // if its the first run we also have to hide the splash screen
      this.nav.setRoot(page)
      .then(() => this.platform.ready())
      .then(() => {

        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        this.statusBar.styleDefault();
        this.splashScreen.hide();
        this.firstRun = false;
      });
    } else {
      this.nav.setRoot(page);
    }
  }
}
