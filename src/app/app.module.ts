import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CallNumber } from '@ionic-native/call-number';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { HomePageModule  } from '../pages/home/home.module';
import { PipesModule } from '../pipes/pipes.module';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { FirebaseuiProvider } from '../providers/firebaseui/firebaseui';
import { AngularFireAuth } from 'angularfire2/auth';

// Declare ga function as ambient
declare let ga: Function;

export const firebaseConfig = {
  apiKey: "AIzaSyCZmVL81yP5nt1HqEXC3YA28dv2rSWtyxs",
  authDomain: "safratwebapp.firebaseapp.com",
  databaseURL: "https://safratwebapp.firebaseio.com",
  projectId: "safratwebapp",
  storageBucket: "safratwebapp.appspot.com",
  messagingSenderId: "779918290139"
};

@NgModule({
  declarations: [
    MyApp,
    LoginPage
    //HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HomePageModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    PipesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InAppBrowser,
    FirebaseuiProvider,
    AngularFireAuth,
    CallNumber,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
