import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { PipesModule } from '../../pipes/pipes.module';
import { IonicImageViewerModule } from 'ionic-img-viewer';

@NgModule({
  declarations: [
     HomePage
    ],
  imports: [IonicPageModule.forChild(HomePage),
            PipesModule,
            IonicImageViewerModule],
})
export class HomePageModule { 
  constructor() {
  }
}