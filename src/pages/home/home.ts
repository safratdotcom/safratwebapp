import { Component, Renderer, ElementRef, ViewChild } from '@angular/core';
import { Content, NavParams, NavController } from 'ionic-angular';
import { IonicPage } from 'ionic-angular';
import { InAppBrowser , InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormGroup, FormControl } from '@angular/forms';
import { CallNumber } from '@ionic-native/call-number';
import * as firebase from 'firebase';
import 'rxjs/add/operator/map';
// Declare ga function as ambient
declare let ga: Function;

@IonicPage({
  segment: 'feedback/:cell'
})

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  toursCollection: AngularFirestoreCollection<Tour>;
  allToursCollection: AngularFirestoreCollection<Tour>;
  tours: Observable<Tour[]>;
  allTours: Observable<Tour[]>;

  public priceRange: number;
  public expanded: any;
  public detailsTab: any;

  public userDisplayName: string="";
  public userPhotoURL: string="";

  start = 0;
  threshold = 100;
  slideHeaderPrevious = 0;
  ionScroll:any;
  showheader:boolean;
  hideheader:boolean;
  headercontent:any;    

   // Retrieve all template reference variables
   @ViewChild(Content) content : Content;
   @ViewChild('panel1') panel1 : ElementRef;
   @ViewChild('panel2') panel2 : ElementRef;
   @ViewChild('panel3') panel3 : ElementRef;
   @ViewChild('panel4') panel4 : ElementRef;
   @ViewChild('panel5') panel5 : ElementRef;
   @ViewChild('panel6') panel6 : ElementRef;
   @ViewChild('panel7') panel7 : ElementRef;
   @ViewChild('panel8') panel8 : ElementRef;
   @ViewChild('panel9') panel9 : ElementRef;
   @ViewChild('panel10') panel10 : ElementRef;
   
   options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
  };

   public cell: number;
   public selectedOrderType: OrderType;
   public orderByLabel: string; 
   public selectedCountryName: string;
   public orderByForm: FormGroup;
   
   public toursArray= [];
   public allToursCount: number = 0;
   public iraqToursCount: number = 0;
   public turkeyToursCount: number = 0;
   public uaeToursCount: number = 0;
   public egyptToursCount: number = 0;
   public lebanonToursCount: number = 0;
   public malysiaToursCount: number = 0;
   public iranToursCount: number = 0;
   public tunisToursCount: number = 0;
   
   public panelPos1 : number;
   public panelPos2 : number;
   public panelPos3 : number;
   public panelPos4 : number;
   public panelPos5 : number;
   public panelPos6 : number;
   public panelPos7 : number;
   public panelPos8 : number;
   public panelPos9 : number;
   public panelPos10 : number;
   

   public panels 	  : any     = [1,2,3,4,5,6,7,8,9,10];
   public callMeFormHidden: boolean = true;
   public orderByFormHidden: boolean = true;
   public priceRangeHidden: boolean = true;

   constructor(public navParams: NavParams, public navCtrl: NavController, private theInAppBrowser: InAppBrowser, 
               public renderer: Renderer ,public myElement: ElementRef,
               private callNumber: CallNumber, 
               private afs: AngularFirestore, private afAuth: AngularFireAuth){
    this.showheader =false;
    this.hideheader = true;

    this.afAuth.authState.subscribe(user => {
      if(user) {
        this.userDisplayName = user.displayName;
        this.userPhotoURL = user.photoURL;
      }
    });            
    this.cell = this.navParams.get('cell');
    this.expanded= [];
    this.priceRange= 2000;
    this.orderByLabel = 'ترتيب وفقا لأختيارنا';

    this.orderByForm = new FormGroup({
      listOptions: new FormControl()
     });
  }

  ngOnInit() {
    // Ionic scroll element
    this.ionScroll = this.myElement.nativeElement.getElementsByClassName('scroll-content')[0];

    // On scroll function
    this.ionScroll.addEventListener("scroll", () => {
    if(this.ionScroll.scrollTop - this.start > this.threshold) {
      this.showheader =true;
      this.hideheader = false;
    } else {
      this.showheader =false;
      this.hideheader = true;
    }

    if (this.slideHeaderPrevious >= this.ionScroll.scrollTop - this.start) {
      this.showheader =false;
      this.hideheader = true;
    }

    this.slideHeaderPrevious = this.ionScroll.scrollTop - this.start;
    });
  }

  logout() {
    firebase.auth().signOut();
  }

  login() {
  }

  callJoint() {
    this.callNumber.isCallSupported()
    .then(function (response) {
        if (response == true) {
          this.callNumber.callNumber(`0049176232`, true);
          console.log("call is supported");
        }
        else {
          console.log("call is not supported");
        }
    });
  }

  public sendLog(index, type) {
    this.afs.collection('logs').add({
                      'action': type, 
                      'tour': this.toursArray[index].ad_link, 
                      'provider': this.toursArray[index].provider_name,
                      'countryName': this.toursArray[index].tour_country, 
                      'orderType': OrderType[this.selectedOrderType],
                      'position': index+1, 
                      'timestamp': Date(), 
                      'userDisplayName': this.userDisplayName,
                      'userPhotoURL': this.userPhotoURL, 
                      'value': 0}
    );
  }

  public toggleShwoingPriceRange() {
    this.priceRangeHidden = !this.priceRangeHidden;
    this.scrollToTop();
  }
  public hideCallMeForm() {
    this.callMeFormHidden = true;
  }
  public showCallMeForm() {
    this.callMeFormHidden = false;
  }
  public toggleShowingCallMeForm(index) {
    console.log(this.toursArray[index]);    
    this.showCallMeForm();
    console.log('inside toggleShowingCallMeForm')
  }
  public toggleShowingTourDetails(index) {
    this.detailsTab= 'tour';
    this.expanded[index] = true;
    this.sendLog(index, "click_tour_details");
  }

  public toggleShowingProviderDetails(index) {
    this.detailsTab= 'provider';
    this.expanded[index] = true;
    this.sendLog(index, "click_company_details");    
  }


  public setExpansionToFalse(index) {
    this.expanded[index] = false;
  }

  ionViewWillEnter() {
    this.selectedOrderType = OrderType.orderByDefault;
    this.getCountryTours('الكل');    
    console.log("is started...");
  }

  public onSliderChanged() {
    this.selectedOrderType= OrderType.noOrder;
    this.orderByLabel = 'ترتيب وفقا لأختيارنا';    
    this.getCountryTours(this.selectedCountryName); 
    ga('send', 'event', {
      eventCategory: 'filter',
      eventLabel: 'pricerange',
      eventAction: 'priceRangeChange',
      eventValue: 10
    });  
  }

  public setOrderByPrice() {
    this.selectedOrderType = OrderType.orderByPrice;
    this.priceRange=2000; 
    this.getCountryTours(this.selectedCountryName);
    this.scrollToTop();
    this.orderByLabel = 'ترتيب وفقا للسعر';   
    ga('send', 'event', {
      eventCategory: 'explore',
      eventLabel: 'sort',
      eventAction: 'setOrderByPrice',
      eventValue: 10
    }); 
  }

  public setOrderByDate() {
    this.selectedOrderType = OrderType.orderByDate;
    this.priceRange=2000;
    this.getCountryTours(this.selectedCountryName);  
    this.scrollToTop();
    this.orderByLabel = 'ترتيب وفقا للموعد';  
    ga('send', 'event', {
      eventCategory: 'explore',
      eventLabel: 'sort',
      eventAction: 'setOrderByDate',
      eventValue: 10
    });    
  }

  public setOrderByDuration() {
    this.selectedOrderType = OrderType.orderByDuration;
    this.priceRange=2000;  
    this.getCountryTours(this.selectedCountryName);
    this.scrollToTop();
    this.orderByLabel = 'ترتيب وفقا للمدة';
    ga('send', 'event', {
      eventCategory: 'explore',
      eventLabel: 'sort',
      eventAction: 'setOrderByDuration',
      eventValue: 10
    });     
  }

  public setOrderByProvider() {
    this.selectedOrderType = OrderType.orderByProvider;
    this.priceRange=2000;  
    this.getCountryTours(this.selectedCountryName);
    this.scrollToTop();
    this.orderByLabel = 'ترتيب وفقا للشركة';
    ga('send', 'event', {
      eventCategory: 'explore',
      eventLabel: 'sort',
      eventAction: 'setOrderByProvider',
      eventValue: 10
    });     
  }

  public setOrderByStars() {
    this.selectedOrderType = OrderType.orderByStars;
    this.priceRange=2000;  
    this.getCountryTours(this.selectedCountryName);
    this.scrollToTop();
    this.orderByLabel = 'ترتيب وفقا للنجوم';
    ga('send', 'event', {
      eventCategory: 'explore',
      eventLabel: 'sort',
      eventAction: 'setOrderByStars',
      eventValue: 10
    });     
  }
  
  public getCountryTours(country: string) {
    this.selectedCountryName = country;
    this.expanded= [];
    if (this.selectedCountryName == 'الكل') {
      switch(this.selectedOrderType) {
        case OrderType.orderByDefault:
         this.toursCollection = this.afs.collection('ads', ref => ref
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false));
          console.log("ordered By Default");
        break;
        case OrderType.orderByPrice:
          this.toursCollection = this.afs.collection('ads', ref => ref
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false)
          .orderBy("tour_price")); 
          console.log("ordered By Price");
        break;
        case OrderType.orderByDuration:
          this.toursCollection = this.afs.collection('ads', ref => ref
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false)
          .orderBy("tour_duration")); 
         console.log("ordered By Duration");
        break;
        case OrderType.orderByDate:
          this.toursCollection = this.afs.collection('ads', ref => ref
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false)
          .orderBy("tour_date"));
         console.log("ordered By Date");
        break;
        case OrderType.orderByProvider:
          this.toursCollection = this.afs.collection('ads', ref => ref
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false)
          .orderBy("provider_name"));
         console.log("ordered By Provider");
        break;  
        case OrderType.orderByStars:
          this.toursCollection = this.afs.collection('ads', ref => ref
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false)
          .orderBy("tour_stars"));
         console.log("ordered By Stars");
        break;        
        case OrderType.noOrder:
        this.toursCollection = this.afs.collection('ads', ref => ref
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false)
          .where('tour_price', '<=', this.priceRange));
        console.log("ordered By Date");       
      }
      this.tours = this.toursCollection.valueChanges();
      this.toursArray=[];
      this.tours.subscribe(tours => {tours.forEach(tour => {this.toursArray.push(tour)})});

      this.allToursCollection = this.afs.collection('ads', ref => ref
      .where('ad_is_published', '==', true)
      .where('ad_is_expired', '==', false)
      .where('tour_price', '<=', this.priceRange));

      this.allTours = this.allToursCollection.valueChanges();        
      this.allToursCount = 0;
      this.iraqToursCount = 0;
      this.turkeyToursCount = 0;
      this.uaeToursCount = 0;
      this.egyptToursCount = 0;
      this.lebanonToursCount = 0;
      this.malysiaToursCount = 0;
      this.iranToursCount = 0;
      this.tunisToursCount = 0; 
      this.allTours.subscribe(allTours => {allTours.forEach(tour => {
        this.allToursCount= this.allToursCount + 1;
        if (tour.tour_country == "العراق") this.iraqToursCount = this.iraqToursCount +1;
        if (tour.tour_country == "تركيا") this.turkeyToursCount = this.turkeyToursCount +1;
        if (tour.tour_country == "الامارات") this.uaeToursCount = this.uaeToursCount +1;
        if (tour.tour_country == "مصر") this.egyptToursCount = this.egyptToursCount +1;
        if (tour.tour_country == "لبنان") this.lebanonToursCount = this.lebanonToursCount +1;
        if (tour.tour_country == "ماليزيا") this.malysiaToursCount = this.malysiaToursCount +1;
        if (tour.tour_country == "ايران") this.iranToursCount = this.iranToursCount +1;
        if (tour.tour_country == "تونس") this.tunisToursCount = this.tunisToursCount +1;
                                                            }
                                                  )
                                    }
                          );
      this.scrollToTop();     
      console.log('inside getAllTours');
      /*ga('send', 'event', {
        eventCategory: 'filter',
        eventLabel: 'setCountry',
        eventAction: 'الكل',
        eventValue: 10
      }); */ 
    } else {
    switch(this.selectedOrderType) {
      case OrderType.orderByDefault:
        this.toursCollection = this.afs.collection('ads', ref => ref
          .where('tour_country', '==', country)
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false));
          console.log("ordered By Default");
      break;
      case OrderType.orderByPrice:
        this.toursCollection = this.afs.collection('ads', ref => ref
          .where('tour_country', '==', country)
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false)
          .orderBy("tour_price")); 
        console.log("ordered By Price");
      break;
      case OrderType.orderByDuration:
        this.toursCollection = this.afs.collection('ads', ref => ref
          .where('tour_country', '==', country)
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false)
          .orderBy("tour_days")); 
        console.log("ordered By Duration");
      break;
      case OrderType.orderByProvider:
        this.toursCollection = this.afs.collection('ads', ref => ref
          .where('tour_country', '==', country)
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false)
          .orderBy("provider_name"));
        console.log("ordered By Provider");
      break;  
      case OrderType.orderByStars:
        this.toursCollection = this.afs.collection('ads', ref => ref
          .where('tour_country', '==', country)
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false)
          .orderBy("hotel_stars"));
        console.log("ordered By Stars");
      break;   
      case OrderType.orderByDate:
        this.toursCollection = this.afs.collection('ads', ref => ref
          .where('tour_country', '==', country)
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false)
          .orderBy("tour_date"));
        console.log("ordered By Date");
      break; 
      case OrderType.noOrder:
        this.toursCollection = this.afs.collection('ads', ref => ref
          .where('tour_country', '==', country)
          .where('ad_is_published', '==', true)
          .where('ad_is_expired', '==', false)
          .where('tour_price', '<=', this.priceRange));
        console.log("ordered By Date");       
    }

      this.tours = this.toursCollection.valueChanges();
      this.toursArray=[];
      this.tours.subscribe(tours => {tours.forEach(tour => {this.toursArray.push(tour)})});
                  
      this.allToursCollection = this.afs.collection('ads', ref => ref
        .where('ad_is_published', '==', true)
        .where('ad_is_expired', '==', false)
        .where('tour_price', '<=', this.priceRange));
      this.allTours = this.allToursCollection.valueChanges();        
      this.allToursCount = 0;
      this.iraqToursCount = 0;
      this.turkeyToursCount = 0;
      this.uaeToursCount = 0;
      this.egyptToursCount = 0;
      this.lebanonToursCount = 0;
                        this.malysiaToursCount = 0;
                        this.iranToursCount = 0;
                        this.tunisToursCount = 0; 
                        this.allTours.subscribe(allTours => {allTours.forEach(tour => {
                          this.allToursCount= this.allToursCount + 1;
                          if (tour.tour_country == "العراق") this.iraqToursCount = this.iraqToursCount +1;
                          if (tour.tour_country == "تركيا") this.turkeyToursCount = this.turkeyToursCount +1;
                          if (tour.tour_country == "الامارات") this.uaeToursCount = this.uaeToursCount +1;
                          if (tour.tour_country == "مصر") this.egyptToursCount = this.egyptToursCount +1;
                          if (tour.tour_country == "لبنان") this.lebanonToursCount = this.lebanonToursCount +1;
                          if (tour.tour_country == "ماليزيا") this.malysiaToursCount = this.malysiaToursCount +1;
                          if (tour.tour_country == "ايران") this.iranToursCount = this.iranToursCount +1;
                          if (tour.tour_country == "تونس") this.tunisToursCount = this.tunisToursCount +1;
                                                                              }
                                                                    )
                                                      }
                                            );
                                      
    this.scrollToTop();     
    console.log('inside getCountryTours');
    ga('send', 'event', {
      eventCategory: 'filter',
      eventLabel: 'setCountry',
      eventAction: country,
      eventValue: 10
    }); 
    }
  }
  
   ionViewDidLoad() : void {
      this.panelPos1 = this.panel1.nativeElement.getBoundingClientRect().top,
      this.panelPos2 = this.panel2.nativeElement.getBoundingClientRect().top,
      this.panelPos3 = this.panel3.nativeElement.getBoundingClientRect().top,
      this.panelPos4 = this.panel4.nativeElement.getBoundingClientRect().top,
      this.panelPos5 = this.panel5.nativeElement.getBoundingClientRect().top;
      this.panelPos6 = this.panel6.nativeElement.getBoundingClientRect().top;
      this.panelPos7 = this.panel7.nativeElement.getBoundingClientRect().top;
      this.panelPos8 = this.panel8.nativeElement.getBoundingClientRect().top;
      this.panelPos9 = this.panel9.nativeElement.getBoundingClientRect().top;
      this.panelPos10 = this.panel10.nativeElement.getBoundingClientRect().top;         
   }

   ionViewDidEnter(): void {  
      this.scrollToPanel(+this.cell);
   }


   public scrollTo(x: number, y: number, duration: number) : void {
      this.content.scrollTo(x, y, duration);
   }

   public scrollToPanel(num : number) : void {
      switch(num)
      {
         case 1:
           this.scrollTo(0, this.panelPos1, 750);
         break;

         case 2:
           this.scrollTo(0, this.panelPos2, 750);
         break;

         case 3:
           this.scrollTo(0, this.panelPos3, 750);
         break;

         case 4:
           this.scrollTo(0, this.panelPos4, 750);
         break;

         case 5:
           this.scrollTo(0, this.panelPos5, 750);
         break;

         case 6:
          this.scrollTo(0, this.panelPos6, 750);
         break;

         case 7:
          this.scrollTo(0, this.panelPos7, 750);
         break;

         case 8:
          this.scrollTo(0, this.panelPos8, 750);
         break;

         case 9:
          this.scrollTo(0, this.panelPos9, 750);
         break;

         case 10:
          this.scrollTo(0, this.panelPos10, 750);
         break;
      }

   }

   public openWithSystemBrowser(url : string){
    let target = "_system";
    this.theInAppBrowser.create(url,target,this.options);
  }
  public openWithInAppBrowser(url : any){
    let target = "_blank";
    this.theInAppBrowser.create(url,target,this.options);
  }

   public scrollToTop() : void {
      this.content.scrollToTop(750);
   }

   public scrollToBottom(): void {
      this.content.scrollToBottom(750);
   }

   public splitDescription(theString: string) {
    return theString.split('.');
   }

   public isNew(insertTimeStamp: number) {
    var date = new Date();
    var currentTimestamp = date.getTime();
    var duration: number = currentTimestamp - insertTimeStamp;
    console.log(duration);
    return (true); //duration < 172800000
   }

   public onOrderChange($event){
    console.log("inside onCountryChange()"); 
    switch(parseInt($event)) {
      case 1: {
        this.setOrderByPrice();
        console.log("1-setOrderByPrice");
        break; 
      }
      case 2: {
        this.setOrderByDuration();
        console.log("2-setOrderByDuration");
        break; 
      }
      case 3: {
        this.setOrderByDate();
        console.log("3-setOrderByDate");
        break; 
      }
      case 4: {
        this.setOrderByProvider();
        console.log("4-setOrderByProvider");
        break; 
      }
      case 5: {
        this.setOrderByStars();
        console.log("5-setOrderByStars");
        break; 
      }
    }
   }

   public onCountryChange($event){
    console.log("inside onCountryChange()"); 
    switch(parseInt($event)) {
      case 1: {
        this.getCountryTours('الكل');
        console.log("1");
        break; 
      }
      case 2: {
        this.getCountryTours('العراق');
        console.log("2");
        break; 
      }
      case 3: {
        this.getCountryTours('تركيا');
        console.log("3");
        break; 
      }
      case 4: {
        this.getCountryTours('الامارات');
        console.log("4");
        break; 
      }
      case 5: {
        this.getCountryTours('مصر');
        console.log("5");
        break; 
      }
      case 6: {
        this.getCountryTours('لبنان');
        console.log("6");
        break; 
      }
      case 7: {
        this.getCountryTours('ماليزيا');
        console.log("7");
        break; 
      }
      case 8: {
        this.getCountryTours('ايران');
        console.log("8");
        break; 
      }
      case 9: {
        this.getCountryTours('تونس');
        console.log("9-تونس");
        break; 
      }
    } 
   }
}

interface Tour {
  ad_insert_date: Date;
  ad_is_published: Boolean;
  ad_is_expired: Boolean;
  ad_priority: number;
  ad_link: string,
  tour_poster: string;
  tour_price: number;
  tour_duration: number;
  tour_country: string;
  tour_cities: string[];
  tour_dests: string[];
  tour_stars: number;
  tour_hotels: string[];
  tour_summary: string;
  ad_tags: string[];
  tour_transporter: string;
  tour_date: Date;
  provider_name: string;
  provider_address: string;
}


enum OrderType {
  noOrder,
  orderByDefault,
  orderByPrice,
  orderByDuration,
  orderByDate,
  orderByProvider,
  orderByStars,
  orderByPriority
}