export class PromotedTour {
    constructor (
        public is_expired: boolean,
        public is_published: boolean,
        public provider_address: string,
        public provider_image: string,
        public provider_link: string,
        public provider_name: string,
        public tour_city: string,
        public tour_country: string,
        public tour_days: number,
        public tour_description: string,
        public tour_expiration_date: string,
        public tour_image: string,
        public tour_link: string,
        public tour_order: number,
        public tour_price: number,
        public tour_type: string) {
    }
}