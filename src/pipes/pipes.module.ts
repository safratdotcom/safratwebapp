import { NgModule } from '@angular/core';
import { SafePipe } from './safe/safe';
import { PrettyPrintPipe } from './prettyPrint/prettyPrint'
@NgModule({
	declarations: [SafePipe, PrettyPrintPipe],
	imports: [],
	exports: [SafePipe, PrettyPrintPipe]
})
export class PipesModule {}