import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'prettyPrint'
})

export class PrettyPrintPipe implements PipeTransform {
    constructor() {

    }

    transform(str: string) {
        let replaced = str.replace(/(\r\n|\r|\n|_b)/g, '<br/>');
        let array = replaced.split("<br/>");

        for(let el of array) {
            if(!!el === false) {
                array.splice(array.indexOf(el), 1);
            }
        }

        return "<p>" + array.join("</p><p>") + "</p>";
        //return "aaaaaaaaaaaaaaaaaaaa";
    }
 
}