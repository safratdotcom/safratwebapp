webpackJsonp([0],{

/***/ 192:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 192;

/***/ }),

/***/ 233:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/home/home.module": [
		234
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 233;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_img_viewer__ = __webpack_require__(471);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]
            ],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_4_ionic_img_viewer__["a" /* IonicImageViewerModule */]],
        }),
        __metadata("design:paramtypes", [])
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_firestore__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map__ = __webpack_require__(248);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomePage = /** @class */ (function () {
    function HomePage(navParams, navCtrl, theInAppBrowser, renderer, myElement, callNumber, afs, afAuth) {
        var _this = this;
        this.navParams = navParams;
        this.navCtrl = navCtrl;
        this.theInAppBrowser = theInAppBrowser;
        this.renderer = renderer;
        this.myElement = myElement;
        this.callNumber = callNumber;
        this.afs = afs;
        this.afAuth = afAuth;
        this.userDisplayName = "";
        this.userPhotoURL = "";
        this.start = 0;
        this.threshold = 100;
        this.slideHeaderPrevious = 0;
        this.options = {
            location: 'yes',
            hidden: 'no',
            clearcache: 'yes',
            clearsessioncache: 'yes',
            zoom: 'yes',
            hardwareback: 'yes',
            mediaPlaybackRequiresUserAction: 'no',
            shouldPauseOnSuspend: 'no',
            closebuttoncaption: 'Close',
            disallowoverscroll: 'no',
            toolbar: 'yes',
            enableViewportScale: 'no',
            allowInlineMediaPlayback: 'no',
            presentationstyle: 'pagesheet',
            fullscreen: 'yes',
        };
        this.toursArray = [];
        this.allToursCount = 0;
        this.iraqToursCount = 0;
        this.turkeyToursCount = 0;
        this.uaeToursCount = 0;
        this.egyptToursCount = 0;
        this.lebanonToursCount = 0;
        this.malysiaToursCount = 0;
        this.iranToursCount = 0;
        this.tunisToursCount = 0;
        this.panels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        this.callMeFormHidden = true;
        this.orderByFormHidden = true;
        this.priceRangeHidden = true;
        this.showheader = false;
        this.hideheader = true;
        this.afAuth.authState.subscribe(function (user) {
            if (user) {
                _this.userDisplayName = user.displayName;
                _this.userPhotoURL = user.photoURL;
            }
        });
        this.cell = this.navParams.get('cell');
        this.expanded = [];
        this.priceRange = 2000;
        this.orderByLabel = 'ترتيب وفقا لأختيارنا';
        this.orderByForm = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormGroup */]({
            listOptions: new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]()
        });
    }
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        // Ionic scroll element
        this.ionScroll = this.myElement.nativeElement.getElementsByClassName('scroll-content')[0];
        // On scroll function
        this.ionScroll.addEventListener("scroll", function () {
            if (_this.ionScroll.scrollTop - _this.start > _this.threshold) {
                _this.showheader = true;
                _this.hideheader = false;
            }
            else {
                _this.showheader = false;
                _this.hideheader = true;
            }
            if (_this.slideHeaderPrevious >= _this.ionScroll.scrollTop - _this.start) {
                _this.showheader = false;
                _this.hideheader = true;
            }
            _this.slideHeaderPrevious = _this.ionScroll.scrollTop - _this.start;
        });
    };
    HomePage.prototype.logout = function () {
        __WEBPACK_IMPORTED_MODULE_7_firebase__["auth"]().signOut();
    };
    HomePage.prototype.login = function () {
    };
    HomePage.prototype.callJoint = function () {
        this.callNumber.isCallSupported()
            .then(function (response) {
            if (response == true) {
                this.callNumber.callNumber("0049176232", true);
                console.log("call is supported");
            }
            else {
                console.log("call is not supported");
            }
        });
    };
    HomePage.prototype.sendLog = function (index, type) {
        this.afs.collection('logs').add({
            'action': type,
            'tour': this.toursArray[index].ad_link,
            'provider': this.toursArray[index].provider_name,
            'countryName': this.toursArray[index].tour_country,
            'orderType': OrderType[this.selectedOrderType],
            'position': index + 1,
            'timestamp': Date(),
            'userDisplayName': this.userDisplayName,
            'userPhotoURL': this.userPhotoURL,
            'value': 0
        });
    };
    HomePage.prototype.toggleShwoingPriceRange = function () {
        this.priceRangeHidden = !this.priceRangeHidden;
        this.scrollToTop();
    };
    HomePage.prototype.hideCallMeForm = function () {
        this.callMeFormHidden = true;
    };
    HomePage.prototype.showCallMeForm = function () {
        this.callMeFormHidden = false;
    };
    HomePage.prototype.toggleShowingCallMeForm = function (index) {
        console.log(this.toursArray[index]);
        this.showCallMeForm();
        console.log('inside toggleShowingCallMeForm');
    };
    HomePage.prototype.toggleShowingTourDetails = function (index) {
        this.detailsTab = 'tour';
        this.expanded[index] = true;
        this.sendLog(index, "click_tour_details");
    };
    HomePage.prototype.toggleShowingProviderDetails = function (index) {
        this.detailsTab = 'provider';
        this.expanded[index] = true;
        this.sendLog(index, "click_company_details");
    };
    HomePage.prototype.setExpansionToFalse = function (index) {
        this.expanded[index] = false;
    };
    HomePage.prototype.ionViewWillEnter = function () {
        this.selectedOrderType = OrderType.orderByDefault;
        this.getCountryTours('الكل');
        console.log("is started...");
    };
    HomePage.prototype.onSliderChanged = function () {
        this.selectedOrderType = OrderType.noOrder;
        this.orderByLabel = 'ترتيب وفقا لأختيارنا';
        this.getCountryTours(this.selectedCountryName);
        ga('send', 'event', {
            eventCategory: 'filter',
            eventLabel: 'pricerange',
            eventAction: 'priceRangeChange',
            eventValue: 10
        });
    };
    HomePage.prototype.setOrderByPrice = function () {
        this.selectedOrderType = OrderType.orderByPrice;
        this.priceRange = 2000;
        this.getCountryTours(this.selectedCountryName);
        this.scrollToTop();
        this.orderByLabel = 'ترتيب وفقا للسعر';
        ga('send', 'event', {
            eventCategory: 'explore',
            eventLabel: 'sort',
            eventAction: 'setOrderByPrice',
            eventValue: 10
        });
    };
    HomePage.prototype.setOrderByDate = function () {
        this.selectedOrderType = OrderType.orderByDate;
        this.priceRange = 2000;
        this.getCountryTours(this.selectedCountryName);
        this.scrollToTop();
        this.orderByLabel = 'ترتيب وفقا للموعد';
        ga('send', 'event', {
            eventCategory: 'explore',
            eventLabel: 'sort',
            eventAction: 'setOrderByDate',
            eventValue: 10
        });
    };
    HomePage.prototype.setOrderByDuration = function () {
        this.selectedOrderType = OrderType.orderByDuration;
        this.priceRange = 2000;
        this.getCountryTours(this.selectedCountryName);
        this.scrollToTop();
        this.orderByLabel = 'ترتيب وفقا للمدة';
        ga('send', 'event', {
            eventCategory: 'explore',
            eventLabel: 'sort',
            eventAction: 'setOrderByDuration',
            eventValue: 10
        });
    };
    HomePage.prototype.setOrderByProvider = function () {
        this.selectedOrderType = OrderType.orderByProvider;
        this.priceRange = 2000;
        this.getCountryTours(this.selectedCountryName);
        this.scrollToTop();
        this.orderByLabel = 'ترتيب وفقا للشركة';
        ga('send', 'event', {
            eventCategory: 'explore',
            eventLabel: 'sort',
            eventAction: 'setOrderByProvider',
            eventValue: 10
        });
    };
    HomePage.prototype.setOrderByStars = function () {
        this.selectedOrderType = OrderType.orderByStars;
        this.priceRange = 2000;
        this.getCountryTours(this.selectedCountryName);
        this.scrollToTop();
        this.orderByLabel = 'ترتيب وفقا للنجوم';
        ga('send', 'event', {
            eventCategory: 'explore',
            eventLabel: 'sort',
            eventAction: 'setOrderByStars',
            eventValue: 10
        });
    };
    HomePage.prototype.getCountryTours = function (country) {
        var _this = this;
        this.selectedCountryName = country;
        this.expanded = [];
        if (this.selectedCountryName == 'الكل') {
            switch (this.selectedOrderType) {
                case OrderType.orderByDefault:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false); });
                    console.log("ordered By Default");
                    break;
                case OrderType.orderByPrice:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false)
                        .orderBy("tour_price"); });
                    console.log("ordered By Price");
                    break;
                case OrderType.orderByDuration:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false)
                        .orderBy("tour_duration"); });
                    console.log("ordered By Duration");
                    break;
                case OrderType.orderByDate:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false)
                        .orderBy("tour_date"); });
                    console.log("ordered By Date");
                    break;
                case OrderType.orderByProvider:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false)
                        .orderBy("provider_name"); });
                    console.log("ordered By Provider");
                    break;
                case OrderType.orderByStars:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false)
                        .orderBy("tour_stars"); });
                    console.log("ordered By Stars");
                    break;
                case OrderType.noOrder:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false)
                        .where('tour_price', '<=', _this.priceRange); });
                    console.log("ordered By Date");
            }
            this.tours = this.toursCollection.valueChanges();
            this.toursArray = [];
            this.tours.subscribe(function (tours) { tours.forEach(function (tour) { _this.toursArray.push(tour); }); });
            this.allToursCollection = this.afs.collection('ads', function (ref) { return ref
                .where('ad_is_published', '==', true)
                .where('ad_is_expired', '==', false)
                .where('tour_price', '<=', _this.priceRange); });
            this.allTours = this.allToursCollection.valueChanges();
            this.allToursCount = 0;
            this.iraqToursCount = 0;
            this.turkeyToursCount = 0;
            this.uaeToursCount = 0;
            this.egyptToursCount = 0;
            this.lebanonToursCount = 0;
            this.malysiaToursCount = 0;
            this.iranToursCount = 0;
            this.tunisToursCount = 0;
            this.allTours.subscribe(function (allTours) {
                allTours.forEach(function (tour) {
                    _this.allToursCount = _this.allToursCount + 1;
                    if (tour.tour_country == "العراق")
                        _this.iraqToursCount = _this.iraqToursCount + 1;
                    if (tour.tour_country == "تركيا")
                        _this.turkeyToursCount = _this.turkeyToursCount + 1;
                    if (tour.tour_country == "الامارات")
                        _this.uaeToursCount = _this.uaeToursCount + 1;
                    if (tour.tour_country == "مصر")
                        _this.egyptToursCount = _this.egyptToursCount + 1;
                    if (tour.tour_country == "لبنان")
                        _this.lebanonToursCount = _this.lebanonToursCount + 1;
                    if (tour.tour_country == "ماليزيا")
                        _this.malysiaToursCount = _this.malysiaToursCount + 1;
                    if (tour.tour_country == "ايران")
                        _this.iranToursCount = _this.iranToursCount + 1;
                    if (tour.tour_country == "تونس")
                        _this.tunisToursCount = _this.tunisToursCount + 1;
                });
            });
            this.scrollToTop();
            console.log('inside getAllTours');
            /*ga('send', 'event', {
              eventCategory: 'filter',
              eventLabel: 'setCountry',
              eventAction: 'الكل',
              eventValue: 10
            }); */
        }
        else {
            switch (this.selectedOrderType) {
                case OrderType.orderByDefault:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('tour_country', '==', country)
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false); });
                    console.log("ordered By Default");
                    break;
                case OrderType.orderByPrice:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('tour_country', '==', country)
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false)
                        .orderBy("tour_price"); });
                    console.log("ordered By Price");
                    break;
                case OrderType.orderByDuration:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('tour_country', '==', country)
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false)
                        .orderBy("tour_days"); });
                    console.log("ordered By Duration");
                    break;
                case OrderType.orderByProvider:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('tour_country', '==', country)
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false)
                        .orderBy("provider_name"); });
                    console.log("ordered By Provider");
                    break;
                case OrderType.orderByStars:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('tour_country', '==', country)
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false)
                        .orderBy("hotel_stars"); });
                    console.log("ordered By Stars");
                    break;
                case OrderType.orderByDate:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('tour_country', '==', country)
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false)
                        .orderBy("tour_date"); });
                    console.log("ordered By Date");
                    break;
                case OrderType.noOrder:
                    this.toursCollection = this.afs.collection('ads', function (ref) { return ref
                        .where('tour_country', '==', country)
                        .where('ad_is_published', '==', true)
                        .where('ad_is_expired', '==', false)
                        .where('tour_price', '<=', _this.priceRange); });
                    console.log("ordered By Date");
            }
            this.tours = this.toursCollection.valueChanges();
            this.toursArray = [];
            this.tours.subscribe(function (tours) { tours.forEach(function (tour) { _this.toursArray.push(tour); }); });
            this.allToursCollection = this.afs.collection('ads', function (ref) { return ref
                .where('ad_is_published', '==', true)
                .where('ad_is_expired', '==', false)
                .where('tour_price', '<=', _this.priceRange); });
            this.allTours = this.allToursCollection.valueChanges();
            this.allToursCount = 0;
            this.iraqToursCount = 0;
            this.turkeyToursCount = 0;
            this.uaeToursCount = 0;
            this.egyptToursCount = 0;
            this.lebanonToursCount = 0;
            this.malysiaToursCount = 0;
            this.iranToursCount = 0;
            this.tunisToursCount = 0;
            this.allTours.subscribe(function (allTours) {
                allTours.forEach(function (tour) {
                    _this.allToursCount = _this.allToursCount + 1;
                    if (tour.tour_country == "العراق")
                        _this.iraqToursCount = _this.iraqToursCount + 1;
                    if (tour.tour_country == "تركيا")
                        _this.turkeyToursCount = _this.turkeyToursCount + 1;
                    if (tour.tour_country == "الامارات")
                        _this.uaeToursCount = _this.uaeToursCount + 1;
                    if (tour.tour_country == "مصر")
                        _this.egyptToursCount = _this.egyptToursCount + 1;
                    if (tour.tour_country == "لبنان")
                        _this.lebanonToursCount = _this.lebanonToursCount + 1;
                    if (tour.tour_country == "ماليزيا")
                        _this.malysiaToursCount = _this.malysiaToursCount + 1;
                    if (tour.tour_country == "ايران")
                        _this.iranToursCount = _this.iranToursCount + 1;
                    if (tour.tour_country == "تونس")
                        _this.tunisToursCount = _this.tunisToursCount + 1;
                });
            });
            this.scrollToTop();
            console.log('inside getCountryTours');
            ga('send', 'event', {
                eventCategory: 'filter',
                eventLabel: 'setCountry',
                eventAction: country,
                eventValue: 10
            });
        }
    };
    HomePage.prototype.ionViewDidLoad = function () {
        this.panelPos1 = this.panel1.nativeElement.getBoundingClientRect().top,
            this.panelPos2 = this.panel2.nativeElement.getBoundingClientRect().top,
            this.panelPos3 = this.panel3.nativeElement.getBoundingClientRect().top,
            this.panelPos4 = this.panel4.nativeElement.getBoundingClientRect().top,
            this.panelPos5 = this.panel5.nativeElement.getBoundingClientRect().top;
        this.panelPos6 = this.panel6.nativeElement.getBoundingClientRect().top;
        this.panelPos7 = this.panel7.nativeElement.getBoundingClientRect().top;
        this.panelPos8 = this.panel8.nativeElement.getBoundingClientRect().top;
        this.panelPos9 = this.panel9.nativeElement.getBoundingClientRect().top;
        this.panelPos10 = this.panel10.nativeElement.getBoundingClientRect().top;
    };
    HomePage.prototype.ionViewDidEnter = function () {
        this.scrollToPanel(+this.cell);
    };
    HomePage.prototype.scrollTo = function (x, y, duration) {
        this.content.scrollTo(x, y, duration);
    };
    HomePage.prototype.scrollToPanel = function (num) {
        switch (num) {
            case 1:
                this.scrollTo(0, this.panelPos1, 750);
                break;
            case 2:
                this.scrollTo(0, this.panelPos2, 750);
                break;
            case 3:
                this.scrollTo(0, this.panelPos3, 750);
                break;
            case 4:
                this.scrollTo(0, this.panelPos4, 750);
                break;
            case 5:
                this.scrollTo(0, this.panelPos5, 750);
                break;
            case 6:
                this.scrollTo(0, this.panelPos6, 750);
                break;
            case 7:
                this.scrollTo(0, this.panelPos7, 750);
                break;
            case 8:
                this.scrollTo(0, this.panelPos8, 750);
                break;
            case 9:
                this.scrollTo(0, this.panelPos9, 750);
                break;
            case 10:
                this.scrollTo(0, this.panelPos10, 750);
                break;
        }
    };
    HomePage.prototype.openWithSystemBrowser = function (url) {
        var target = "_system";
        this.theInAppBrowser.create(url, target, this.options);
    };
    HomePage.prototype.openWithInAppBrowser = function (url) {
        var target = "_blank";
        this.theInAppBrowser.create(url, target, this.options);
    };
    HomePage.prototype.scrollToTop = function () {
        this.content.scrollToTop(750);
    };
    HomePage.prototype.scrollToBottom = function () {
        this.content.scrollToBottom(750);
    };
    HomePage.prototype.splitDescription = function (theString) {
        return theString.split('.');
    };
    HomePage.prototype.isNew = function (insertTimeStamp) {
        var date = new Date();
        var currentTimestamp = date.getTime();
        var duration = currentTimestamp - insertTimeStamp;
        console.log(duration);
        return (true); //duration < 172800000
    };
    HomePage.prototype.onOrderChange = function ($event) {
        console.log("inside onCountryChange()");
        switch (parseInt($event)) {
            case 1: {
                this.setOrderByPrice();
                console.log("1-setOrderByPrice");
                break;
            }
            case 2: {
                this.setOrderByDuration();
                console.log("2-setOrderByDuration");
                break;
            }
            case 3: {
                this.setOrderByDate();
                console.log("3-setOrderByDate");
                break;
            }
            case 4: {
                this.setOrderByProvider();
                console.log("4-setOrderByProvider");
                break;
            }
            case 5: {
                this.setOrderByStars();
                console.log("5-setOrderByStars");
                break;
            }
        }
    };
    HomePage.prototype.onCountryChange = function ($event) {
        console.log("inside onCountryChange()");
        switch (parseInt($event)) {
            case 1: {
                this.getCountryTours('الكل');
                console.log("1");
                break;
            }
            case 2: {
                this.getCountryTours('العراق');
                console.log("2");
                break;
            }
            case 3: {
                this.getCountryTours('تركيا');
                console.log("3");
                break;
            }
            case 4: {
                this.getCountryTours('الامارات');
                console.log("4");
                break;
            }
            case 5: {
                this.getCountryTours('مصر');
                console.log("5");
                break;
            }
            case 6: {
                this.getCountryTours('لبنان');
                console.log("6");
                break;
            }
            case 7: {
                this.getCountryTours('ماليزيا');
                console.log("7");
                break;
            }
            case 8: {
                this.getCountryTours('ايران');
                console.log("8");
                break;
            }
            case 9: {
                this.getCountryTours('تونس');
                console.log("9-تونس");
                break;
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */])
    ], HomePage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('panel1'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "panel1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('panel2'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "panel2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('panel3'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "panel3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('panel4'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "panel4", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('panel5'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "panel5", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('panel6'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "panel6", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('panel7'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "panel7", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('panel8'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "panel8", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('panel9'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "panel9", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('panel10'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "panel10", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/awitwit/ionicProjects/safrat/src/pages/home/home.html"*/'<html lang="en" dir="rtl">\n<ion-header [ngClass]="{\'hide-header\':showheader,\'show-header\':hideheader}">\n  <ion-navbar align-title="center">\n    <div style="font-family: \'sky-bold\';  font-size: 1.7rem; background-color: #577eb9; ">\n      <ion-title text-center>\n        سفرات      \n      </ion-title>\n      <!--<div style="font-family: \'sky-bold\';  font-size: 1.7rem; background-color: #577eb9; color:#FFFFFF; float: right;">\n        <button (click)="logout()" style="font-family: \'sky-bold\';  font-size: 1.7rem; background-color: #577eb9; color:#FFFFFF;">\n          <ion-icon name="exit"></ion-icon>\n        </button>\n      </div>\n      <div style="font-family: \'sky-bold\';  font-size: 1.7rem; background-color: #577eb9; color:#FFFFFF; float: left;">\n          <button (click)="showOrderByForm()" style="font-family: \'sky-bold\';  font-size: 1.7rem; background-color: #577eb9; color:#FFFFFF;">\n              <ion-icon name="funnel"></ion-icon>              \n          </button>\n        </div>-->\n    </div>\n    <!--<div style="display: inline-block;">\n     <ion-item style="max-width:200px; float:left;">\n      <ion-avatar item-start>\n        <img src="{{userPhotoURL}}">\n      </ion-avatar>\n      <button ion-button (click)="logout()">\n          الغاء الانضمام\n      </button> \n     </ion-item>\n    </div>-->\n    <!--<div style="background-color: #577eb9;">\n      <ion-segment [(ngModel)]="country">\n        <ion-segment-button value="all" style="color:#FFFFFF;" (click)="getCountryTours(\'الكل\')"><b>الكل</b></ion-segment-button>                            \n        <ion-segment-button value="iraq" style="color:#FFFFFF;" (click)="getCountryTours(\'العراق\')"><b>العراق</b></ion-segment-button>                \n        <ion-segment-button value="turkey" style="color:#FFFFFF;" (click)="getCountryTours(\'تركيا\')"><b>تركيا</b></ion-segment-button>\n        <ion-segment-button value="uae" style="color:#FFFFFF;" (click)="getCountryTours(\'الامارات\')"><b>الامارات</b></ion-segment-button>\n        <ion-segment-button value="egypt" style="color:#FFFFFF;" (click)="getCountryTours(\'مصر\')"><b>مصر</b></ion-segment-button>\n        <ion-segment-button value="lebeanon" style="color:#FFFFFF;" (click)="getCountryTours(\'لبنان\')"><b>لبنان</b></ion-segment-button>    \n        <ion-segment-button value="malysia" style="color:#FFFFFF;" (click)="getCountryTours(\'ماليزيا\')"><b>ماليزيا</b></ion-segment-button>      \n        <ion-segment-button value="iran" style="color:#FFFFFF;" (click)="getCountryTours(\'ايران\')"><b>ايران</b></ion-segment-button>            \n      </ion-segment> \n    </div>-->\n  </ion-navbar>\n</ion-header>\n<ion-content style="background-color: #FFFFFF;">\n    <ion-grid text-right>\n      <div> <!--  *ngIf="priceRangeHidden != true" -->\n        <div>\n          <img width="100%" src="assets/img/banner.jpg"> \n        </div>\n        <ion-card class="card" style="margin-top: -50px;">\n\n        <ion-row no-padding>   \n          <ion-col col-xs-6 col-sm-6 no-padding style="background-color: #263f61; color: #FFFFFF; border-bottom: 5px solid #d40e14;"> \n            <div style="padding: 10px;">الدولة</div>\n            <ion-select name="countrySelected" (ionChange)="onCountryChange($event)" interface="action-sheet" okText="موافق" cancelText="الغاء">\n              <ion-option value="1">الكل ({{ allToursCount }})</ion-option>\n              <ion-option value="2">العراق ({{ iraqToursCount }})</ion-option>\n              <ion-option value="3">تركيا ({{ turkeyToursCount }})</ion-option>\n              <ion-option value="4">الامارات ({{ uaeToursCount }})</ion-option>\n              <ion-option value="5">مصر ({{ egyptToursCount }})</ion-option>\n              <ion-option value="6">لبنان ({{ lebanonToursCount }})</ion-option>\n              <ion-option value="7">ماليزيا ({{ malysiaToursCount }})</ion-option>\n              <ion-option value="8">ايران ({{ iranToursCount }})</ion-option>  \n              <ion-option value="9">تونس ({{ tunisToursCount }})</ion-option>                            \n            </ion-select>\n          </ion-col> \n          <ion-col col-xs-6 col-sm-6 no-padding style="background-color: #263f61; color: #FFFFFF; border-bottom: 5px solid #d40e14; border-right:2px solid #C0C0C0;"> \n            <div style="padding: 10px;">الترتيب</div>\n            <ion-select name="orderSelected" (ionChange)="onOrderChange($event)" interface="action-sheet" okText="موافق" cancelText="الغاء">\n              <ion-option value="1">السعر</ion-option>\n              <ion-option value="2">المدة</ion-option>\n              <ion-option value="3">الموعد</ion-option>   \n              <ion-option value="4">الشركة</ion-option>            \n              <ion-option value="5">النجوم</ion-option>                          \n            </ion-select>\n          </ion-col> \n        </ion-row>  \n        \n        <ion-row>\n          <ion-col col-xs-12 col-sm-12 style="text-align: center; padding-top: 20px;">\n              <span>السعر لغاية </span>\n              <span>{{priceRange}} $</span>\n              <span> ( </span>\n              <span>{{ allToursCount }}</span>  \n              <span>سفرة</span>   \n              <span> ) </span>                            \n              <ion-item>\n                <ion-range dir="ltr" min="0" max="2000" step="100" [(ngModel)]="priceRange" debounce="1200" (ionChange)="onSliderChanged()">\n                </ion-range>\n              </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n      </div>\n\n      <div *ngFor="let tour of tours |  async; let i = index">\n        \n        <!-- *************************************   Ahmed Alaa Card:start ********************************************************** -->\n        <!--<div class="ahcontainer">\n            <div class="ahcard">\n              <div class="ahcard__hero" style="background: url(\'assets/img/{{ tour.tour_poster }}\')">\n                <div class="ahcard__hero__info">\n                  <span class="ahcard__hero__info__price">${{ tour.tour_price }}</span>\n                  <span>{{ tour.tour_days }} ايام - {{ tour.tour_days-1 }} ليالي</span>\n                </div> \n                <img src="assets/img/{{ tour.tour_poster }}">\n              </div>\n              <div class="ahcard__body">\n                <div class="ahcard__body__title"> \n                  {{ tour.tour_country }}&nbsp;(&nbsp;{{ tour.tour_cities }}&nbsp;)\n                </div> \n                  <div class="ahcard__body__text">\n                    {{tour.tour_summary | slice:0:300}}                  \n                    <span class="ahcard__body__text__date">{{tour.tour_date.toDate() | date:\'yyyy-MM-dd\'}}</span>\n                  </div> \n                <div class="ahcard__body__info">\n                  <div class="ahcard__body__info__company">\n                    <b>{{ tour.tour_provider_name }}</b>\n                    <div>{{ tour.tour_provider_address }}</div>\n                  </div>\n                  <div class="ahcard__body__info__accommodation">\n                    <b>{{ tour.tour_hotels }}</b>\n                    <div>{{ tour.tour_airline}}</div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>-->\n        <!-- *************************************   Ahmed Alaa Card:end ********************************************************** -->\n        <ion-card calss="card">\n            <ion-card-content style="margin: 0 !important; padding: 0 !important;">\n              <ion-row no-padding>\n                  <ion-col col-xs-12 col-sm-3 no-padding>\n                    <div style="cursor:pointer;">\n                      <img src="assets/img/{{ tour.tour_poster }}" imageViewer>\n                      <div *ngIf="isNew(tour.ad_insert_date)" class="card_elem--newlabel">جديد</div>\n                      <div class="card_elem--imageOverlayLabel">\n                        <span class="card_elem--imageOverlayLabel--price" style="float: right">${{ tour.tour_price }}</span>\n                        <span style="float: left">{{ tour.tour_duration }} ايام - {{ tour.tour_duration-1 }} ليالي</span>\n                      </div>\n                    </div> \n                  </ion-col>\n                  <ion-col col-xs-12 col-sm-5 (click)="toggleShowingTourDetails(i)" style="padding-right: 10px; cursor:pointer;">\n                    <div style="padding-right: 10px; padding-top: 10px; color:#092a5e"><b>{{ tour.tour_country }}&nbsp;(&nbsp;{{ tour.tour_cities }}&nbsp;)</b></div>\n                    <div style="padding-right: 10px;">\n                      <span *ngIf="tour.tour_stars == 1" style="color: #f6ab3f">&#9733;</span>\n                      <span *ngIf="tour.tour_stars == 2" style="color: #f6ab3f">&#9733;&#9733;</span>\n                      <span *ngIf="tour.tour_stars == 3" style="color: #f6ab3f">&#9733;&#9733;&#9733;</span>\n                      <span *ngIf="tour.tour_stars == 4" style="color: #f6ab3f">&#9733;&#9733;&#9733;&#9733;</span>\n                      <span *ngIf="tour.tour_stars == 5" style="color: #f6ab3f">&#9733;&#9733;&#9733;&#9733;&#9733;</span>   \n                    </div>  \n                    <div style="padding-right: 10px;">{{ tour.tour_hotels }}</div>                    \n                    <div class="card__elem--tourdescription">{{tour.tour_summary | slice:0:300}}</div>\n                    <div style="padding-right: 10px;">\n                      <span *ngFor="let tag of tour.ad_tags">\n                        <span *ngIf="tag == \'excellent_program\'" class="card__elem--isExcellentTourDetails">\n                         برنامج سياحي مميز                                               \n                        </span>\n                        <span *ngIf="tag == \'excellent_hotels\'" class="card__elem--isExcellentTourDetails">\n                         فنادق راقية                                              \n                        </span>\n                        <span *ngIf="tag == \'excellent_dests\'" class="card__elem--isExcellentTourDetails">\n                         وجهات رائعة                        \n                        </span>\n                      </span>                    \n                    </div>  \n                  </ion-col> \n                  <ion-col col-xs-12 col-sm-4 (click)="toggleShowingProviderDetails(i)" style="padding-right: 10px; cursor:pointer;">\n                    <div style="padding-right: 10px;">\n                      <div style="padding-top: 6px;">\n                        <ion-item no-padding style="color:#092a5e; font-size:14px;">\n                            <ion-avatar item-left> <img src="assets/img/{{tour.provider_icon}}"> </ion-avatar>\n                            <b>{{ tour.tour_provider_name }}</b>\n                        </ion-item>\n                      </div>\n                      <div class="card__elem--provideraddress">{{ tour.tour_provider_address }}</div>  \n                      <div style="padding-top: 5px;">\n                        <div>{{ tour.tour_transporter}}</div>\n                        <div>{{ tour.tour_price }} دولار</div>\n                        <div>{{ tour.tour_duration }} ايام / {{ tour.tour_duration-1 }} ليالي</div>\n                        <div>{{tour.tour_date.toDate() | date:\'yyyy-MM-dd\'}}</div>\n                      </div>  \n                    </div>\n                  </ion-col>    \n              </ion-row>\n              <div *ngIf="expanded[i] != true" style="text-align: center; cursor:pointer; color:#575757">\n                  <div (click)="toggleShowingTourDetails(i)">المزيد من التفاصيل</div>\n              </div>  \n            </ion-card-content>  \n        </ion-card>\n        <div *ngIf="expanded[i] == true">\n          <ion-card>\n            <button ion-item (click)="setExpansionToFalse(i)" style="text-align: center;">اغلاق</button>\n            <ion-segment [(ngModel)]="detailsTab">\n              <ion-segment-button value="tour" style="color:#363738;"><b>السفرة</b></ion-segment-button>\n              <ion-segment-button value="destination" style="color:#363738;"><b>الوجهة</b></ion-segment-button>                                \n              <ion-segment-button value="provider" style="color:#363738;"><b>الشركة</b></ion-segment-button>\n            </ion-segment>\n            <div [ngSwitch]="detailsTab">\n              <div *ngSwitchCase="\'tour\'">\n                <div *ngFor="let line of splitDescription(tour.tour_details)">  \n                  <div class="card__elem--tourdetails">{{ line }}</div>\n                </div>\n                <iframe [src]="tour.ad_link | safe" width="500" height="672" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>\n              </div>\n              <!-- <div *ngSwitchCase="\'destination\'">\n                  <div *ngFor="let destination of tour.tour_destinations">  \n                    <div *ngIf="destination == \'الحمامات\'">الحمامات\n                      <br>\n                      <div class="video-container">\n                        <iframe width="420" height="315" frameborder="0" src="https://www.youtube.com/embed/XqZFrRxUxZs"></iframe>\n                      </div>\n                    </div>      \n                    <div *ngIf="destination == \'سوسة\'">سوسة\n                        <br>\n                        <div class="video-container">\n                          <iframe width="420" height="315" frameborder="0" src="https://www.youtube.com/embed/5KAdf0SfQg4"></iframe>\n                        </div>\n                    </div>   \n                    <div *ngIf="destination == \'جامع عقبة بن نافع\'">جامع عقبة بن نافع\n                        <br>\n                        <div class="video-container">\n                          <iframe width="420" height="315" frameborder="0" src="https://www.youtube.com/embed/beI4BtgtIXo"></iframe>\n                        </div>\n                    </div>                    \n                    <div *ngIf="destination == \'انطاليا\'">انطاليا\n                      <br>\n                      <div class="video-container">\n                        <iframe width="420" height="315" frameborder="0" src="https://www.youtube.com/embed/ofGMFbrWK6M"></iframe>\n                      </div>\n                    </div>  \n                    <div *ngIf="destination == \'الانيا\'">الانيا\n                      <br>                \n                      <div class="video-container">\n                        <iframe width="420" height="315" frameborder="0" src="https://www.youtube.com/embed/suShdgQR43U"></iframe> \n                      </div>\n                    </div>      \n                    <div *ngIf="destination == \'بورصة\'">بورصة\n                      <br>                 \n                      <div class="video-container">\n                        <iframe width="420" height="315" frameborder="0" src="https://www.youtube.com/embed/BldyeC6x5pM"></iframe> \n                      </div>\n                    </div>  \n                    <div *ngIf="destination == \'اسطنبول\'">اسطنبول\n                        <br>                 \n                        <div class="video-container">\n                          <iframe width="420" height="315" frameborder="0" src="https://www.youtube.com/embed/7A1q7v4btbk"></iframe> \n                        </div>\n                    </div> \n                  </div>             \n              </div> -->                                       \n              <div *ngSwitchCase="\'provider\'">\n                <div style="padding-top: 10px;">\n                    <img src="assets/img/{{ tour.provider_image }}" imageViewer>\n                </div>\n                <div style="padding-top: 16px;"><b>{{ tour.provider_name }}</b></div>\n                <div class="card__elem--provideraddress">{{ tour.provider_address }}</div>  \n                  <!-- <button type="button" ion-button color="primary" (click)="callJoint()">Call Number</button> -->\n                  <!--<div *ngFor="let line of splitDescription(tour.provider_details)">  \n                    <div class="card__elem--providerdetails">{{ line }}</div>\n                  </div>-->\n                <div style="padding-top: 10px;">\n                  <iframe [src]="tour.provider_fb_link | safe" width="80%;" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>\n                    <!--<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Forchidgroup.iq&tabs=timeline&width=640&height=500&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId" width="640" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>-->\n                </div>\n                <div *ngFor="let comment of tour.provider_comments">\n                  <div class="card__elem--providerdetails">\n                    <p>👎 {{ comment.comment_date.toDate()  | date:\'yyyy-MM-dd\' }} {{ comment.commenter_name }} </p>\n                    <div *ngFor="let line of comment.comment_content.split(\'.\')">\n                        <p>{{ line }}</p>  \n                    </div>\n                  </div>\n                </div>\n              </div>                         \n            </div>\n            <button ion-item (click)="setExpansionToFalse(i)" style="text-align: center;">اغلاق</button>\n          </ion-card> \n        </div>  \n      </div>\n    </ion-grid> \n\n<div class="pins">\n  <div class="panel white" #panel1></div>    \n  <div class="panel white" #panel2></div>\n  <div class="panel white" #panel3></div>\n  <div class="panel white" #panel4></div>\n  <div class="panel white" #panel5></div>\n  <div class="panel white" #panel6></div>\n  <div class="panel white" #panel7></div>\n  <div class="panel white" #panel8></div>\n  <div class="panel white" #panel9></div>\n  <div class="panel white" #panel10></div>\n</div>\n</ion-content>\n<ion-footer> <!-- [ngClass]="{\'hide-footer\':showheader,\'show-footer\':hideheader}" -->\n  <ion-grid>\n    <ion-row>\n      <ion-col col-xs-4 col-sm-4 no-padding>\n        <button no-margin ion-button full large color="mygrey" (click)="toggleShwoingPriceRange()"  style="font-size: 12px;"> اعلى سعر {{priceRange}} دولار </button>      \n      </ion-col>\n      <ion-col col-xs-4 col-sm-4 no-padding>\n        <button  no-margin ion-button full large color="mygrey" (click)="scrollToTop()" style="font-size: 12px;" ><ion-icon name="arrow-dropup-circle"></ion-icon></button>              \n      </ion-col>\n      <ion-col col-xs-4 col-sm-4 no-padding>          \n        <button no-margin ion-button full large color="mygrey" (click)="scrollToTop()" style="font-size: 12px;">{{ orderByLabel }}</button>\n      </ion-col>\n    </ion-row>\n    <ion-row style="padding: 10px; font-size: 12px; text-align: center; color: black;">\n      <ion-col col-xs-4 col-sm-4 no-padding>\n        <div style="color: #928F8F;" (click)="login()">دخول الشركات</div>\n      </ion-col>\n      <ion-col col-xs-4 col-sm-4 no-padding>\n        <div style="cursor:pointer;" (click)="openWithSystemBrowser(\'https://www.facebook.com/سفرات-227412894540572\')">اتصل بنا</div>          \n      </ion-col>\n      <ion-col col-xs-4 col-sm-4 no-padding>\n        <div style="color: #928F8F;" (click)="logout()">خروج</div>\n      </ion-col>\n    </ion-row>      \n  </ion-grid>  \n</ion-footer>\n</html>'/*ion-inline-end:"/home/awitwit/ionicProjects/safrat/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Renderer */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_firestore__["a" /* AngularFirestore */], __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["a" /* AngularFireAuth */]])
    ], HomePage);
    return HomePage;
}());

var OrderType;
(function (OrderType) {
    OrderType[OrderType["noOrder"] = 0] = "noOrder";
    OrderType[OrderType["orderByDefault"] = 1] = "orderByDefault";
    OrderType[OrderType["orderByPrice"] = 2] = "orderByPrice";
    OrderType[OrderType["orderByDuration"] = 3] = "orderByDuration";
    OrderType[OrderType["orderByDate"] = 4] = "orderByDate";
    OrderType[OrderType["orderByProvider"] = 5] = "orderByProvider";
    OrderType[OrderType["orderByStars"] = 6] = "orderByStars";
    OrderType[OrderType["orderByPriority"] = 7] = "orderByPriority";
})(OrderType || (OrderType = {}));
//# sourceMappingURL=home.js.map

/***/ }),

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__safe_safe__ = __webpack_require__(469);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__prettyPrint_prettyPrint__ = __webpack_require__(470);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__safe_safe__["a" /* SafePipe */], __WEBPACK_IMPORTED_MODULE_2__prettyPrint_prettyPrint__["a" /* PrettyPrintPipe */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__safe_safe__["a" /* SafePipe */], __WEBPACK_IMPORTED_MODULE_2__prettyPrint_prettyPrint__["a" /* PrettyPrintPipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirebaseuiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(248);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebaseui__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebaseui___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebaseui__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the FirebaseuiProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var FirebaseuiProvider = /** @class */ (function () {
    function FirebaseuiProvider() {
        console.log('Hello FirebaseuiProvider Provider');
        // Initialize the FirebaseUI Widget using Firebase.
        this.ui = new __WEBPACK_IMPORTED_MODULE_3_firebaseui__["auth"].AuthUI(__WEBPACK_IMPORTED_MODULE_2_firebase__["auth"]());
    }
    FirebaseuiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], FirebaseuiProvider);
    return FirebaseuiProvider;
}());

//# sourceMappingURL=firebaseui.js.map

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(420);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 420:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(494);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_login_login__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_home_home_module__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pipes_pipes_module__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angularfire2__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_angularfire2_auth__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_angularfire2_firestore__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_firebaseui_firebaseui__ = __webpack_require__(294);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var firebaseConfig = {
    apiKey: "AIzaSyCZmVL81yP5nt1HqEXC3YA28dv2rSWtyxs",
    authDomain: "safratwebapp.firebaseapp.com",
    databaseURL: "https://safratwebapp.firebaseio.com",
    projectId: "safratwebapp",
    storageBucket: "safratwebapp.appspot.com",
    messagingSenderId: "779918290139"
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */]
                //HomePage
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'feedback/:cell', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_10__pages_home_home_module__["HomePageModule"],
                __WEBPACK_IMPORTED_MODULE_12_angularfire2__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
                __WEBPACK_IMPORTED_MODULE_14_angularfire2_firestore__["b" /* AngularFirestoreModule */].enablePersistence(),
                __WEBPACK_IMPORTED_MODULE_13_angularfire2_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_11__pipes_pipes_module__["a" /* PipesModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_15__providers_firebaseui_firebaseui__["a" /* FirebaseuiProvider */],
                __WEBPACK_IMPORTED_MODULE_13_angularfire2_auth__["a" /* AngularFireAuth */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__["a" /* CallNumber */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 469:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SafePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(38);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SafePipe = /** @class */ (function () {
    function SafePipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    SafePipe.prototype.transform = function (url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    };
    SafePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'safe'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]])
    ], SafePipe);
    return SafePipe;
}());

//# sourceMappingURL=safe.js.map

/***/ }),

/***/ 470:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrettyPrintPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PrettyPrintPipe = /** @class */ (function () {
    function PrettyPrintPipe() {
    }
    PrettyPrintPipe.prototype.transform = function (str) {
        var replaced = str.replace(/(\r\n|\r|\n|_b)/g, '<br/>');
        var array = replaced.split("<br/>");
        for (var _i = 0, array_1 = array; _i < array_1.length; _i++) {
            var el = array_1[_i];
            if (!!el === false) {
                array.splice(array.indexOf(el), 1);
            }
        }
        return "<p>" + array.join("</p><p>") + "</p>";
        //return "aaaaaaaaaaaaaaaaaaaa";
    };
    PrettyPrintPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'prettyPrint'
        }),
        __metadata("design:paramtypes", [])
    ], PrettyPrintPipe);
    return PrettyPrintPipe;
}());

//# sourceMappingURL=prettyPrint.js.map

/***/ }),

/***/ 494:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.firstRun = true;
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyCZmVL81yP5nt1HqEXC3YA28dv2rSWtyxs",
            authDomain: "safratwebapp.firebaseapp.com",
            databaseURL: "https://safratwebapp.firebaseio.com",
            projectId: "safratwebapp",
            storageBucket: "safratwebapp.appspot.com",
            messagingSenderId: "779918290139"
        };
        __WEBPACK_IMPORTED_MODULE_4_firebase__["initializeApp"](config);
        this.platform.setDir('rtl', true);
    }
    MyApp.prototype.ngAfterViewInit = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().onAuthStateChanged(function (user) {
            if (user) {
                // User is authenticated.
                _this.setRootPage('HomePage');
                console.log('in HomePage');
            }
            else {
                // User is not authenticated.
                _this.setRootPage('HomePage');
                console.log('in HomePage');
                //this.setRootPage(LoginPage);
                //console.log('in LoginPage');
            }
        });
    };
    MyApp.prototype.setRootPage = function (page) {
        var _this = this;
        if (this.firstRun) {
            // if its the first run we also have to hide the splash screen
            this.nav.setRoot(page)
                .then(function () { return _this.platform.ready(); })
                .then(function () {
                // Okay, so the platform is ready and our plugins are available.
                // Here you can do any higher level native things you might need.
                _this.statusBar.styleDefault();
                _this.splashScreen.hide();
                _this.firstRun = false;
            });
        }
        else {
            this.nav.setRoot(page);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/awitwit/ionicProjects/safrat/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/awitwit/ionicProjects/safrat/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 495:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_firebaseui_firebaseui__ = __webpack_require__(294);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebaseui__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebaseui___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebaseui__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, uiProvider) {
        this.navCtrl = navCtrl;
        this.uiProvider = uiProvider;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
        // The start method will wait until the DOM is loaded.
        this.uiProvider.ui.start('#firebaseui-auth-container', this.getUiConfig());
    };
    LoginPage.prototype.getUiConfig = function () {
        // FirebaseUI config.
        return {
            callbacks: {
                signInSuccess: function (currentUser, credential, redirectUrl) {
                    // Do something.
                    // Return type determines whether we continue the redirect automatically
                    // or whether we leave that to developer to handle.
                    return false;
                }
            },
            credentialHelper: __WEBPACK_IMPORTED_MODULE_4_firebaseui__["auth"].CredentialHelper.NONE,
            signInOptions: [
                // Leave the lines as is for the providers you want to offer your users.
                {
                    provider: __WEBPACK_IMPORTED_MODULE_3_firebase__["auth"].GoogleAuthProvider.PROVIDER_ID,
                    customParameters: {
                        // Forces account selection even when one account 
                        // is available. 
                        prompt: 'select_account'
                    }
                },
                __WEBPACK_IMPORTED_MODULE_3_firebase__["auth"].FacebookAuthProvider.PROVIDER_ID,
                //firebase.auth.TwitterAuthProvider.PROVIDER_ID,
                //firebase.auth.GithubAuthProvider.PROVIDER_ID,
                __WEBPACK_IMPORTED_MODULE_3_firebase__["auth"].EmailAuthProvider.PROVIDER_ID,
            ],
            // Terms of service url.
            tosUrl: '<your-tos-url>'
        };
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/home/awitwit/ionicProjects/safrat/src/pages/login/login.html"*/'<!DOCTYPE html>\n<html>\n<head>\n    <script src="https://cdn.firebase.com/libs/firebaseui/3.0.0/firebaseui.js"></script>\n    <link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/3.0.0/firebaseui.css" />\n</head>\n<body>\n\n  <ion-header>\n    <ion-navbar align-title="center">\n        <div style="font-family: \'sky-bold\';  font-size: 1.7rem; color: #000000;">\n            <ion-title text-center>\n              سفرات      \n            </ion-title>\n          </div>\n    </ion-navbar>\n  </ion-header>  \n\n<ion-content padding>\n  <div style="padding: 30px; font-size: 3rem; text-align: center; color:#240101;">\n    <p>\n      انضم معنا الى سفرات\n    </p>\n    <p>  \n      الموقع الاول لمقارنة عروض شركات السفر والسياحة في العراق   \n    </p>     \n  </div>\n  <div id="firebaseui-auth-container"></div>\n</ion-content>\n</body>\n</html>'/*ion-inline-end:"/home/awitwit/ionicProjects/safrat/src/pages/login/login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_firebaseui_firebaseui__["a" /* FirebaseuiProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[297]);
//# sourceMappingURL=main.js.map